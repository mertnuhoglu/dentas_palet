#!/bin/bash

sudo apt-get update &&

# Install R libraries for reporting and verification

sudo echo 'options(repos=structure(c(CRAN="https://mirror.las.iastate.edu/CRAN/")))' >> "/usr/lib/R/etc/Rprofile.site"
sudo echo 'options(repos=structure(c(CRAN="https://mirror.las.iastate.edu/CRAN/")))' >> "$HOME/.Rprofile"

sudo apt-get -y install r-base &&
sudo apt-get -y install libcurl4-openssl-dev libssl-dev libxml2-dev &&
sudo apt-get install -y libudunits2-dev libgdal-dev libgeos-dev libproj-dev
sudo -i R -e 'install.packages(c("dplyr", "readr", "glue", "jsonlite", "rjson", "WriteXLS", "roxygen2"))' &&

sudo -i R -e 'install.packages(c("shiny", "rmarkdown", "shinydashboard", "usethis", "testthat"))' &&
sudo -i R -e 'install.packages(c("sodium", "curl", "devtools", "shinyjs", "DT"))' &&
sudo -i R -e 'install.packages(c("tidyr", "stringr", "rio"))' &&
sudo apt-get -y install libsodium-dev &&
sudo -i R -e 'devtools::install_github("paulc91/shinyauthr")' &&
sudo -i R -e 'install.packages(c("Rook", "readxl", "plumber"))' &&

# environment variables

echo '
export DENTAS_PALET_PROJECT_DIR="$HOME/dentas_palet"
export DENTAS_PALET_FILES_DIR=$DENTAS_PALET_PROJECT_DIR/out
' >> ~/.bashrc &&
source ~/.bashrc &&
mkdir -p $DENTAS_PALET_FILES_DIR &&
cd DENTAS_PALET_PROJECT_DIR &&
wget https://raw.githubusercontent.com/PaulC91/shinyauthr/master/inst/shiny-examples/shinyauthr_example/returnClick.js -O out/returnClick.js 

