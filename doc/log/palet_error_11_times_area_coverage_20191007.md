
# palet: %1100 alan kullanım hatası 20191007 

## debug dosyalarını oluştur

### f05 order by: cardboard_id, -layout_id, palette_id, formula_id

kolon sıralaması da buna uygun olsun

### Final

Check `/Users/mertnuhoglu/projects/itr/palet/dentas_palet/app/www/log/debug.xlsx`

`51792`

`h06b`

		| cardboard_id | palette_id | layout_no | fits_in_all |
		| 51792-486    | 100*140    | 212       | 1           |
		| 51792-486    | 70*130     | 212       | 1           |

fitting nerede hesaplanıyordu?

`~/projects/itr/palet/dentas_palet/R/calc_area.R`

``` bash
	h06b = fits_in(f04b)
``` 

`~/projects/itr/palet/dentas_palet/R/find_layout.R`

`h06a`

		| 51792-486 | 70*130 | 211 | X2A | FALSE |
		| 51792-486 | 70*130 | 212 | X2B | TRUE  |

`f05b`

		| cardboard_id | palette_id | layout_no | layout_id | lhs_val | rhs_val | fits_in_palette |
		| 51792-486    | 70*130     | 212       | X2B       | 1210    | 1300    | TRUE            |

`f05`

order by: cardboard_id, -layout_id, palette_id, formula_id

`f01`

		| formula          | layout_id | formula_id |
		| IF(D1<=2*B4,1,0) | X2B       | 333        |
		| IF(D2<=B3,1,0)   | X2B       | 334        |


### Error: RHS formülleri yanlış 

`f01`

		IF(D1<=2*B4,1,0)	X2B	333
		IF(D2<=B3,1,0)	X2B	334

`f05`

		51792-486	70*130	X2B	212	334	IF(D2<=B3,1,0)	334	D2<=B3	D2	<=	B3	0	1	1	0
		51792-486	70*130	X2A	211	332	IF(D2<=B4,1,0)	332	D2<=B4	D2	<=	B4	0	1	0	1

`333` silinmiş

`f02` içinde de bu yok.

Sebep:

``` bash
parse_lhs_rhs = function(f01) {
...
		dplyr::filter(!stringr::str_detect(formula, "=\\d")) %>%
``` 

Bunu daha belirleyici bir şekilde tanımlamalıyım.

## n tane paleti yanyana dizmeye izin ver

01: layouts.tsv ve formulas.tsv

`~/projects/itr/palet/dentas_palet/inst/extdata/layouts.tsv`

`~/projects/itr/palet/dentas_palet/inst/extdata/formulas.tsv`

02: images

``` bash
cd /Users/mertnuhoglu/projects/itr/palet/dentas_palet/app/www/img_palette/
cp X2A.jpg X3A.jpg
cp X2A.jpg X4A.jpg
cp X2A.jpg X5A.jpg
cp X2A.jpg X6A.jpg
cp X2A.jpg X7A.jpg
cp X2A.jpg X8A.jpg
cp X2A.jpg X9A.jpg
cd thumbnail
cp X2A.jpg X3A.jpg
cp X2A.jpg X4A.jpg
cp X2A.jpg X5A.jpg
cp X2A.jpg X6A.jpg
cp X2A.jpg X7A.jpg
cp X2A.jpg X8A.jpg
cp X2A.jpg X9A.jpg
``` 

## infizibl durumda layout_not_found.jpg resmini göster

infizibl durumu nasıl tespit edeceğim?

hiçbir `fits_in` olmaması durumunda

h06a tablosu nasıl?

opt01: `anti_join` yap

opt02: filter(==0) sonra ...

### opt01: `anti_join` yap

## sadece 2 tane yanyana konulabilsin

Edit: `layouts.tsv` and `formulas.tsv`

## sağlama yap

55867 sadece fizibl. area coverage 2.6.

2.6 nasıl olabilir ancak?

`cardboard_per_layout` hepsinde tek olacak çok sayıda palet kullanılan yerleşimlerde

### Error: hala eski layouts.tsv dosyasından 291 ve sonrasındaki layoutların bilgileri alınıyor

		X2A	291	2
		X2B	292	2
		X3A	391	3

`~/projects/itr/palet/dentas_palet/inst/extdata/layouts.tsv`

Tekrar okumak gerekiyormuş

``` bash
	ly0 = readr::read_tsv(system.file("extdata", "layouts.tsv", package = "dentaspalet")) 
``` 

### 59063 gerçekten infizibl mı?

## infizbl sonuçları da result tablosunda gösterelim
