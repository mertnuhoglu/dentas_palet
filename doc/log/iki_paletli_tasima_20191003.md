
# palet: 2 paletli taşımayı yapalım 20191003 

Yeni formül eklemeliyiz:

``` bash
cd /Users/mertnuhoglu/Documents/img/thumbnail
mogrify -quality 100 -density 300x300 -resize 100x50 *.jpg
``` 

## error: resimler gelmiyor sunucuda

Lokalde çalışıyor, sunucuda hata var.

opt01: test scripti hazırla

``` bash
usethis::use_test("palette_layout_per_cardboard")
``` 

Edit `~/projects/itr/palet/dentas_palet/tests/testthat/test-palette_layout_per_cardboard.R`

Check `out/outh06b.tsv`

``` bash
cardboard_id	palette_id	layout_no	fits_in_all
cb1     p10             1
``` 

`layout_no` neden boş?

Sorun burada:

``` bash
	h06b = fits_in(f04b)
	g06e = max_area_p_ly_per_cb(h06b, area)
``` 

Sorun g06c'de belirgin:

``` bash
	g06c = h06b %>%
		dplyr::group_by(cardboard_id, palette_id) %>%
		dplyr::summarize(layout_no = max(layout_no))
``` 

``` bash
cardboard_id    palette_id      layout_no
cb1     p10
``` 

Peki o boş satırlar nereden geliyor?

is.na ile bulalım

``` bash
h06b %>%
	filter(is.na(layout_no))
``` 

``` bash
	h06b = fits_in(f04b)
``` 

``` bash
f04b %>%
	filter(is.na(layout_no))
``` 

``` bash
	f04b = prepare_input_data(ref_data, f04)
``` 

``` bash
	filter(f04, is.na(layout_id))
	##> empty
``` 

``` bash
	f04b = f04 %>%
		dplyr::mutate(crossjoin = 1) %>%
		dplyr::full_join(cb1, by = "crossjoin") %>%
		dplyr::full_join(p1, by = "crossjoin") %>%
		dplyr::select(-crossjoin) %>%
		dplyr::left_join(ly1, by = "layout_id") %>%
		dplyr::select(-cardboard_per_layout)
``` 

Bu ancak ly1 ile join'den kaynaklanabilir

``` bash
	ly1 = ref_data$layout
	anti_join(f04, ly1, by = "layout_id")
  ##>   formula layout_id expr_id expr lhs   op    rhs   coef_clength coef_cwidth
  ##>   <chr>   <chr>       <int> <chr> <chr> <chr> <chr>        <dbl>     <dbl>
  ##> 1 =IF(D2… 19A           332 D2<=… D2    <=    B4               0         1
  ##> 2 =IF(D2… 19B           334 D2<=… D2    <=    B3               0         1
``` 

f04 formulas.txt içinden geliyor. ly1 ise layouts.txt

Sebep: layouts `layouts.tsv` dosyasından alınıyor, bense `layout_nos.txt` dosyasını değiştirmişim.

Edit `~/projects/itr/palet/dentas_palet/inst/extdata/layouts.tsv`

``` bash
X2A	211	2
X2B	212	2
``` 

Bu fazlalık dosyaları git'ten kaldır.

``` bash
git mv 19A.jpg X2A.jpg 
git mv 19B.jpg X2B.jpg 
cd thumbnail
git mv 19A.jpg X2A.jpg 
git mv 19B.jpg X2B.jpg 

``` 

Error: 6: tibble::tibble(formula = frm, layout_id = sid) at parse_formulas.R#8

Sorun yeri:

``` bash
	frm = readLines(system.file("extdata", "formulas02.txt", package = "dentaspalet")) 
	sid = readLines(system.file("extdata", "layout_ids.txt", package = "dentaspalet")) 
``` 

Bu iki dosyayı paste ile birleştirmeliydim.

``` bash
echo 'formula\tlayout_id' > inst/extdata/formulas.tsv 
paste -d '\t' inst/extdata/formulas02.txt inst/extdata/layout_ids.txt >> inst/extdata/formulas.tsv 
``` 

``` bash
f01 = readr::read_tsv("inst/extdata/formulas.tsv")
``` 

Ancak hala 19A ve 19B için hata veriyor

