
# Selim'in Sağlama Örnekleri 20191009

> [12:22, 09.10.2019] Selim Kayacan: ilk satıra bakıyorum
> [12:22, 09.10.2019] Selim Kayacan: diye bi çıktı var
> [12:22, 09.10.2019] Selim Kayacan: Cardboard ölçüleri yaklaşık 89 cm * 99 cm
> [12:22, 09.10.2019] Selim Kayacan: Palet ölçüsü 90*100
> [12:23, 09.10.2019] Selim Kayacan: tek palete sığar, ki matematiksel olarak sıgmıs da (98% doluluk vermişiz)
> [12:23, 09.10.2019] Selim Kayacan: ama çizim modeli ve palet modeline baktııgmda çift palet degil mi o ?
> [12:23, 09.10.2019] Selim Kayacan: neden öyle çıkmış o kısım?
> [12:26, 09.10.2019] Selim Kayacan: bi de şu var
> [12:28, 09.10.2019] Selim Kayacan: burda da sıkıntı şu bence :  ben içeri 800*1200  gibi bi palet atmışım , ama bizim algoritma gitmiş 90*100' koymus , o da sığmamış çift palet yapmış , ama dolulugu da %91 hesaplamış 😳

debug.xlsx dosyasını indir:

``` bash
scp itr01:~/dentas_palet/app/www/log/debug.xlsx .
``` 

## incele01

		cardboard_id	palette_id	area_covered	layout_id	layout_img	cardboard_side_a	cardboard_side_b
		52C35-024	90*100	 0.98	X2B	<a href="img_palette/X2B.jpg" target="_blank"><img src="img_palette/thumbnail/X2B.jpg"/></a>	887	992

`h06c`: bu karton şu palet ve layoutlara uymuş:

		| cardboard_id | palette_id | layout_no | fits_in_all |
		| 52C35-024    | 100*120    | 101       | 1           |
		| 52C35-024    | 100*120    | 291       | 1           |
		| 52C35-024    | 100*120    | 292       | 1           |
		| 52C35-024    | 100*140    | 101       | 1           |
		| 52C35-024    | 100*140    | 291       | 1           |
		| 52C35-024    | 100*140    | 292       | 1           |
		| 52C35-024    | 70*130     | 292       | 1           |
		| 52C35-024    | 80*120     | 292       | 1           |
		| 52C35-024    | 90*100     | 101       | 1           |
		| 52C35-024    | 90*100     | 291       | 1           |
		| 52C35-024    | 90*100     | 292       | 1           |

`g06c`: bunların arasından en yüksek layout_no olanlar bunlar. bir seferde en çok kartonu sığdırmaya çalışıyor bizim algoritma.

		| cardboard_id | palette_id | layout_no |
		| 52C35-024    | 100*120    | 292       |
		| 52C35-024    | 100*140    | 292       |
		| 52C35-024    | 70*130     | 292       |
		| 52C35-024    | 80*120     | 292       |
		| 52C35-024    | 90*100     | 292       |

Mert:

> dediğin gibi, tek palete sığıyor. bunlar fizibl. ancak bizim algoritma mantık olarak, bir layout içindeki karton sayısını maksimize etmeye çalışıyor. bunun nedeni, adamların bize verdikleri excel dosyasının bu mantığı kullanıyor oluşu. 
> ancak acaba birden çok paletli durumlarda, bu kural kullanılmamalı mı? onu bilemiyorum. o kuralın mantığı neydi en başta? ona göre karar verelim.

### 52D03-297 örneği

`result`

``` bash
  ##> "52D03-297  90*100   0.91  X2B  <a href=""img_palette/X2B.jpg"" target=""_blank""><img src=""img_palette/thumbnail/X2B.jpg""/></a>  734  1115"
``` 

`h06c`: fizibl yerleşimler:

``` bash
  ##> "52D03-297  100*120  101  1"
  ##> "52D03-297  100*120  291  1"
  ##> "52D03-297  100*120  292  1"
  ##> "52D03-297  100*140  101  1"
  ##> "52D03-297  100*140  291  1"
  ##> "52D03-297  100*140  292  1"
  ##> "52D03-297  70*130   292  1"
  ##> "52D03-297  80*120   101  1"
  ##> "52D03-297  80*120   291  1"
  ##> "52D03-297  80*120   292  1"
  ##> "52D03-297  90*100   291  1"
  ##> "52D03-297  90*100   292  1"
``` 

`g06c`

``` bash
  ##> "52D03-297  100*120  292"
  ##> "52D03-297  100*140  292"
  ##> "52D03-297  70*130   292"
  ##> "52D03-297  80*120   292"
  ##> "52D03-297  90*100   292"
``` 

## tek paletli yerleşimleri iki paletli yerleşimlere tercih edelim

Edit `~/projects/itr/palet/dentas_palet/inst/extdata/layouts.tsv`

``` bash
X2A	91	1
X2B	92	1
``` 

## error: çift palet yerleşimlerde area_covered 2'ye bölünmeli 20191010 

Palet sayısını da tutmamız lazım bu durumda layouts.tsv tablosunda. 

## 01A ve diğer resimleri tekrar aktar

``` bash
mogrify -quality 100 -density 300x300 -resize 100x50 01A.jpg
mogrify -quality 100 -density 300x300 -resize 100x50 X2A.jpg
mogrify -quality 100 -density 300x300 -resize 100x50 X2B.jpg
``` 

## çok paletli yerleşimleri ekle

``` bash
mogrify -quality 100 -density 300x300 -resize 100x50 X4C.jpg
mogrify -quality 100 -density 300x300 -resize 100x50 X4B.jpg
mogrify -quality 100 -density 300x300 -resize 100x50 X4A.jpg
mogrify -quality 100 -density 300x300 -resize 100x50 X3A.jpg
``` 

## 10 cm taşma izni 20191010

### Requirements

https://mail.yandex.com.tr/?uid=1130000023670156#message/170292360909949925

> Normal palet çalışmalarında
> 
> ð  Palet eninden 10cm’ye kadar taşma olabilir. İki taraftan toplam
> 
> ð  Palet boyundan 10cm’ye kadar taşma olabilir. İki taraftan toplam
> 
> ð  Palet eninden 5cm’den fazla içerde kalmamalıyız. İki taraftan toplam
> 
> ð  Palet boyundan 15cm’den fazla içerde kalmamalıyız. İki taraftan toplam
> 
> Bunun dışında Çorlu fabrikada inline işlerde 120cm’den fazla palet boyu olan paletleri kullanamayalım gibi bir kısıt var. Bu şekilde başka kısıtlar çıkabilir.
> 
> İlk palet çalışmasında müşterinin taşmalı/taşmasız seçeneği olmalı (taşmasız seçildiğinde yukarda belirttiğimiz 10cm’ye kadar taşma konusu 0 olmuş olacak)

`fits_overflow` diye yeni bir değişken ekleyelim

``` bash
	, fits_overflow = lhs_val <= (rhs_val + 100)
``` 

diğeri için yaptığımız tüm hesapları bunun için de yapalım
 
`fits_in_all_of` diye bir değişken daha ekleyelim

``` bash
	h06bof = h06a %>%
		dplyr::group_by(cardboard_id, palette_id, layout_no) %>%
		dplyr::summarize( fits_in_all_of = min(fits_overflow)) 
``` 

`g06c` sıralamasında `overflow_allowed` başta gelsin

#### Refactoring: rename outf05.tsv as f05.tsv and for all other tsv files

#### error: neden layout_no boş

#### overflow_allowed için nasıl bir çıktı üreteceğim?

### palet: area_covered doğru mu? 20191011 

``` bash
scp itr01:~/dentas_palet/app/www/log/debug.xlsx .
``` 




