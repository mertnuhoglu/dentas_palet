# D1 -> D1*1 yapalım
# bunun için D1 etrafında hiçbir katsayı olmadığından emin olalım
# örneğin: 

pattern = "(?<!\\d{1,2}\\s{0,10}\\*\\s{0,10})D1(?!\\s{0,10}\\*\\s{0,10}\\d{1,2})"
stringr::str_detect("D1+D2", pattern)
#> [1] TRUE
stringr::str_detect("2*D1+D2", pattern)
#> [1] FALSE
stringr::str_detect("D1 * 2+D2", pattern)
#> [1] FALSE

f02 = rio::import("out03.xlsx") %>%
	mutate(coef_d1_right = case_when(
      stringr::str_detect(lhs, "(?<!\\d{1,2}\\s{0,10}\\*\\s{0,10})D1(?!\\s{0,10}\\*\\s{0,10}\\d{1,2})") ~ 1
		)) 
  ##>                       input            expr       lhs op   rhs coef_d1_right
  ##> 1           =IF(D1<=B3,1,0)          D1<=B3        D1 <=    B3             1
  ##> 2           =IF(D2<=B4,1,0)          D2<=B4        D2 <=    B4            NA
  ##> 3         =IF(D1*2<=B3,1,0)        D1*2<=B3      D1*2 <=    B3            NA
  ##> 4           =IF(D2<=B4,1,0)          D2<=B4        D2 <=    B4            NA
  ##> 5           =IF(D1<=B3,1,0)          D1<=B3        D1 <=    B3             1
