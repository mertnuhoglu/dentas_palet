library(dplyr)

f01 = readLines("formulas01.txt") 
  ##> =IF(D1<=B3,1,0)
  ##> =IF(D2<=B4,1,0)
  ##> =IF(B10+B11=2,101)
  ##> =IF(D1*2<=B3,1,0)

f02 = tibble::tibble( input = f01 ) %>%
	# filter out lines such as: =IF(B10+B11=2,101)
	filter(!str_detect(input, "=\\d")) %>%
	# extract formula from: =IF(D1<=B3,1,0) ==> D1<=B3
	mutate(expr = stringr::str_match(input, "\\(([^,]*),([^)]*)\\)")[,2])
f02
  ##>    input             expr
  ##>    <chr>             <chr>
  ##>  1 =IF(D1<=B3,1,0)   D1<=B3
  ##>  2 =IF(D2<=B4,1,0)   D2<=B4
  ##>  3 =IF(D1*2<=B3,1,0) D1*2<=B3

