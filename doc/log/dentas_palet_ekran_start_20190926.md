
# others

## sunucu yanıt vermiyor 20190929 

GCP ödemesi yapılmadığından sunucuyu kapatmış.
 
### tekrar kurulum yap

# Dentaş Palet Ekranları Start 20190926 

## README.md dosyasını hazırla

### test scriptini hazırla
 
``` bash
usethis::use_test("palet_calculation")
``` 

Check `~/projects/itr/palet/dentas_palet/tests/testthat/test-palet_calculation.R`

## kağıt üzerinde taslak çizim yap

		https://docs.google.com/spreadsheets/d/19q2p0Ld2CqTRgqWHJ4yLee723-DHdYSn/edit#gid=836382753

    /Users/mertnuhoglu/gdrive/IMG_20190926_090626.jpg

## başlangıç ekranını yap

Check `~/projects/itr/palet/dentas_palet/R/palet_screen.R`

### add dependencies

``` bash
devtools::install_github("paulc91/shinyauthr")
usethis::use_package("dplyr")
usethis::use_package("stringr")
usethis::use_package("tidyr")
usethis::use_package("shiny")
usethis::use_package("shinyjs")
usethis::use_package("shinydashboard")
usethis::use_package("glue")
usethis::use_package("DT")
usethis::use_dev_package("paulc91/shinyauthr")
``` 

### Hatırlatma

File widget: `~/codes/rr/shiny-examples/073-widget-file/server.R`

``` bash
shiny::runApp("073-widget-file")
``` 

Submit button: `~/codes/rr/shiny-examples/079-widget-submit/server.R`

``` bash
shiny::runApp("079-widget-submit")
``` 

### input widgetlarını koy

### observeEvent tanımla

### şimdi de hesaplamayı yapalım

## layouts tablosunu dahili olarak oku görünmesin

## deploy et

önceki gibi yapalım

port: 5060
peymandev.i-terative.com

excel dosyaları oluştur

``` r
rio::convert("/Users/mertnuhoglu/projects/itr/palet/dentas_palet/inst/extdata/palettes.tsv", "/Users/mertnuhoglu/projects/itr/palet/dentas_palet/inst/extdata/palettes.xlsx")
rio::convert("/Users/mertnuhoglu/projects/itr/palet/dentas_palet/inst/extdata/cardboards.tsv", "/Users/mertnuhoglu/projects/itr/palet/dentas_palet/inst/extdata/cardboards.xlsx")
``` 

## layout resimlerini koyalım

``` bash
	img18e = system.file("extdata/img_palette", "18E.JPG", package = "dentaspalet")
	file.copy(img18e, "/Users/mertnuhoglu/projects/itr/palet/dentas_palet/img18e.jpg")
``` 

Resimleri küçült

``` bash
cd /Users/mertnuhoglu/projects/itr/palet/dentas_palet/inst/extdata/img_palette/
mkdir thumbnail
cp *.JPG thumbnail
cd thumbnail
mogrify -quality 100 -density 300x300 -resize 50x20 *.JPG
rename 's/JPG/jpg2/' *.JPG
rename 's/jpg2/jpg/' *.jpg2
``` 

### datatable içine resim koyma

#### resim linklerini koy

``` r
		system.file("extdata/img_palette", "18E.jpg", package = "dentaspalet")
		state$result = palette_layout_per_cardboard(ly0, p0, cb0) %>%
			dplyr::mutate(layout_img = system.file("extdata/img_palette", glue::glue("{layout_id}.jpg"), package = "dentaspalet"))
``` 

bir dosya linki koy shiny sayfasına:

##### error: 404 not found

bir dosya linki koy shiny sayfasına:

opt01: use tags$a

https://stackoverflow.com/questions/39039424/how-to-link-a-local-file-to-an-html-query-in-the-shiny-ui-r

https://subscription.packtpub.com/book/application_development/9781785280900/8/ch08lvl1sec53/the-www-directory

Put files into `www` folder

``` bash
mkdir www
git mv inst/extdata/img_palette/ www
``` 

``` bash
tags$a(href='img_palette/18E.jpg', target='blank', '18E', download = 'img_palette/18E.jpg')
``` 

opt02: localhost ekleyelim

relative path

##### opt05: en basit bir uygulama ile test et

Edit `~/projects/study/r/shiny/ex/study_shiny/ex03/01.R`

``` bash
source("01.R")
``` 

##### opt06: shiny-examples içindeki www kullanımlarını incele

		shiny::runApp("107-events")
		~/codes/rr/shiny-examples/107-events/server.R

Burada çalışıyor:

``` bash
			tags$a(href='plasmid1.txt', target='blank', 'plasmid1_localfile', download = 'plasmid1.txt'),
``` 

opt06a: palette_screen.R kodunu buraya aktar temizleyerek git

opt06b: runApp directory kodu ver 

Check `~/projects/itr/palet/dentas_palet/R/server.R`

Check `~/projects/itr/palet/dentas_palet/app/server.R`

``` bash
shiny::runApp("app")
``` 

Tamam bu şekilde çalıştı

`www` yeri: `~/projects/itr/palet/dentas_palet/app/www/plasmid1.txt`

``` r
				, tags$a(href='plasmid1.txt', target='blank', 'plasmid1_localfile', download = 'plasmid1.txt')
``` 

#### datatables içine image nasıl konuluyor?

direk koy bakalım ne oluyor?

opt01: doğrudan tablo içine <img> elementlerini ekle

https://stackoverflow.com/questions/41784631/include-link-to-local-html-file-in-datatable-in-shiny

``` bash
				#, DT::dataTableOutput("result_table")
``` 

->

``` bash
		state$result = dentaspalet::palette_layout_per_cardboard(ly0, p0, cb0) %>%
			dplyr::mutate(layout_img = '<a href="" target="_blank"><img src="img_palette/18E.jpg"/></a>')
			...

				, DT::renderDataTable({state$result}, escape = F)
``` 

#### eksik olan layoutları çıkart

kıyas yap

``` bash
ly0 = readr::read_tsv(system.file("extdata", "layouts.tsv", package = "dentaspalet"))
d0 = ly0$layout_id
d1 = list.files(path = "/Users/mertnuhoglu/projects/itr/palet/dentas_palet/app/www/img_palette/") %>% str_replace("\\.jpg", "")
setdiff(d0, d1)
  ##> [1] "01A" "02A" "04D" "04E" "06H" "09E" "09F" "12F" "16E"
``` 

##### Eksik resimleri üret

opt01:

https://stackoverflow.com/questions/48612440/how-to-create-image-with-imagemagick

``` bash
wget -O circle.png https://i.stack.imgur.com/gFW3K.png
convert -size 450x234 xc:white \( circle.png -resize 200x200 \) \
-gravity northwest -geometry +100+70 -compose over -composite \
-bordercolor red -border 1 \
\( -size 279x -background none -fill black \
-font arial -pointsize 28 -gravity center \
caption:"Layout not found" -trim +repage \) \
-gravity center -geometry +0-20 -compose over -composite \
layout_not_found.jpg
``` 

``` bash
cp layout_not_found.jpg 01A.jpg
cp layout_not_found.jpg 02A.jpg
cp layout_not_found.jpg 04D.jpg
cp layout_not_found.jpg 04E.jpg
cp layout_not_found.jpg 06H.jpg
cp layout_not_found.jpg 09E.jpg
cp layout_not_found.jpg 09F.jpg
cp layout_not_found.jpg 12F.jpg
cp layout_not_found.jpg 16E.jpg
``` 

Resimlerin tümünü tekrar küçült

``` bash
rm -rf thumbnail
mkdir thumbnail
cp *.jpg thumbnail
mogrify -quality 100 -density 300x300 -resize 100x50 *.jpg
``` 

### deploy

Error:

    cannot open file '/Users/mertnuhoglu/projects/itr/palet/dentas_palet/app/www/returnClick.js': No such file or directory

#### error: resimler sunucuda niye 404 veriyor?

``` bash
35.204.111.216:5060
``` 

Sebep:

Dosyalar sunucuda büyük JPG olarak uzantılı.

``` bash
rename 's/JPG/jpg/' *.JPG
cd thumbnail
rename 's/JPG/jpg/' *.JPG
``` 

Error: dosyalar küçük

Yanlışlıkla dosyaları küçültmüşüm ana klasörde de.

