
# Tümer'in sağlaması 20191011 

https://mail.yandex.com.tr/?uid=1130000023670156#message/170573835886660728?openMessageInList=false

> Ekteki dosyayı ekleyip rastgele denemeler yaptık.
> 
> 53N96-286 no’lu üründeki çözümde minimum içe girmeyi ihlal eden bir çözümle karşılaştık, 53244-245’te ise 10cm taşma kuralını ihlal eden çözümle karşılaştık.
> 
> Bu örnekler üzerinden kurguyu tekrar kontrol etme şansımız olur mu?
> 
> Bir de inline kutular özelinde farklı bir kısıt koyma durumu vardı, onun için ne dersiniz?

53N96-286
53244-245

## Error: Error in format(round(carea/(parea * palette_per_layout), 2), nsmall = 2) :

		Error in format(round(carea/(parea * palette_per_layout), 2), nsmall = 2) :
			object 'palette_per_layout' not found

## 53244-245 taşma ihlali

70*120
06C

		53244-245	70*120	603	06C	1032	1200	TRUE	TRUE
		53244-245	70*120	603	06C	1280	1200	FALSE	TRUE

cardboard_id	palette_id	layout_id	layout_no	formula_id	formula	expr_id	expr	lhs	op	rhs	coef_clength	coef_cwidth	coef_plength	coef_pwidth	clength	cwidth	plength	pwidth	palette_per_layout	crossjoin	lhs_val	rhs_val	fits_in_palette	fits_overflow
53244-245	70*120	06C	603	63	IF(D1*2<=B3,1,0)	63	D1*2<=B3	D1*2	<=	B3	2	0	1	0	516	320	1200	700	1	1	1032	1200	TRUE	TRUE
53244-245	70*120	06C	603	64	IF(D2*4<=B3,1,0)	64	D2*4<=B3	D2*4	<=	B3	0	4	1	0	516	320	1200	700	1	1	1280	1200	FALSE	TRUE

D1*2<=B3	D1*2
D2*4<=B3	D2*4

516	320

1032	1200
1280	1200


