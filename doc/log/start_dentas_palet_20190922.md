
# Dentaş palet projesi 20190922 

Create new bitbucket repo

``` bash
cd /Users/mertnuhoglu/projects/itr/palet
git clone git@bitbucket.org:mertnuhoglu/dentas_palet.git
cd dentas_palet
``` 

## Excel simülasyonu

https://docs.google.com/spreadsheets/d/19q2p0Ld2CqTRgqWHJ4yLee723-DHdYSn/edit#gid=896740960

Sekme: "debug01"

## tüm formülleri otomatik çek excel dosyasından 

Formül metnini almak için: 

		=FORMULATEXT(B12)

Örnek:

		=IF(D1<=B3,1,0)
		=IF(D2<=B4,1,0)
		=IF(B10+B11=2,101)

Mappings

		D1	long_side
		D2	short_side
		B3	palette_length
		B4	palette_width

Formülleri tabloya çevir:

		coef_long_side	coef_short_side	coef_palette_length	coef_palette_width	fits_in
		1	0	1	0	TRUE
		0	1	0	1	TRUE

### otomatik olarak formülleri nasıl tabloya dönüştüreceğim?

#### opt01: regex kullan

mopal.xlsx$Palettenschemaerrechnung$original is source for `formulas01.txt`

Input formulas to be parsed: `~/projects/itr/palet/dentas_palet/doc/log/ex/start_dentas_palet_20190922/e01/formulas01.txt`

Edit `~/projects/itr/palet/dentas_palet/doc/log/ex/start_dentas_palet_20190922/e01/ex01.R`

D1 -> 1 * D1 yapalım

Edit `~/projects/itr/palet/dentas_palet/doc/log/ex/start_dentas_palet_20190922/e01/ex02.R`

#### hatalı formülleri düzeltme:

> [10:11, 23.09.2019] mert nuhoglu: mopal.xlsx dosyasında Palettenschemaerrechnung sayfasında 229. satırda formül şu şekilde girilmiş:
> 
> =IF(D1*4=B3,1,0)
> 
> herhalde = yerine <= olacaktı. yazım hatası olduğunu varsayıyorum. çünkü eşitlik olmasının bir anlamı yok, değil mi?
> [10:15, 23.09.2019] mert nuhoglu: yine 44. satırdaki formül şöyle:
> 
> =IF(D1>B3,1,0)
> 
> > olmasının anlamı var mı yoksa diğerleri gibi <= olması gerekmez mi?
> [10:16, 23.09.2019] mert nuhoglu: 70. satır: =IF(D1>400,1,0) yine aynı tuhaflık

Bu hatalı formüllerdeki `>` ve `=` operatörlerini `<=` ile değiştirdim. mopal.xlsx dokümanında bunları `updated` kolonuna koydum. Buradan kopyaladığım düzeltilmiş formülleri `formulas02.txt` dokümanına besledim.

Check `~/projects/itr/palet/dentas_palet/doc/log/ex/start_dentas_palet_20190922/e01/formulas02.txt`

Edit `~/projects/itr/palet/dentas_palet/doc/log/ex/start_dentas_palet_20190922/e01/ex03.R`

#### önde bulunan bir katsayı var mı?

Edit `~/projects/itr/palet/dentas_palet/doc/log/ex/start_dentas_palet_20190922/e01/ex04.R`

Aslında buna gerek yok, nasıl olsa her türlü bu formülleri buluyoruz.

#### D1 -> D1*1 yapalım

`1` katsayısını explicit olarak ekleyelim.

Edit `~/projects/itr/palet/dentas_palet/doc/log/ex/start_dentas_palet_20190922/e01/ex05.R`

#### tüm coefficient kolonlarını birleştirelim

Edit `~/projects/itr/palet/dentas_palet/doc/log/ex/start_dentas_palet_20190922/e01/ex07.R`

#### d2 için de bunu yapalım

Edit `~/projects/itr/palet/dentas_palet/doc/log/ex/start_dentas_palet_20190922/e01/ex08.R`

### rhs için coef ayrıştırma

#### package oluşturma

Follow `~/projects/study/r/study_packages_in_R.Rmd`

##### dışarıdan alalım datayı

``` bash
mkdir data
mkdir -p inst/extdata
cp ~/projects/itr/palet/dentas_palet/doc/log/ex/start_dentas_palet_20190922/e01/formulas02.txt inst/extdata
``` 

``` bash
mkdir -p inst/scripts
mkdir -p out
``` 

Edit `~/projects/itr/palet/dentas_palet/inst/scripts/main_dentas_palet.R`

#### rhs için coef ayrıştırma

aynı mantıkla yapalım

`~/projects/itr/palet/dentas_palet/R/parse_formulas.R`

## değişken isimleri

``` bash
	f04 = f03 %>%
		dplyr::rename(
			coef_length = coef_d1
			, ..
		)
``` 

## schema_id aktarımı

Input data copied into `~/projects/itr/palet/dentas_palet/inst/extdata/formulas03.txt`

Also into `~/projects/itr/palet/dentas_palet/schema_nos.txt`

Copy paste down using vim macro: 

``` vim
yeVnk:s/^$/1901
/
j/^\d\+
N
``` 

Also into `~/projects/itr/palet/dentas_palet/schema_ids.txt`

``` vim
ma/^\w\+mb"wye'aV'b:s/^$/w'bmjma
``` 

## hangisi long_side onu belirle ve hesaplamaları yap

## fits_in formülü

## toplu kullanım main fonksiyonu

Check `~/projects/itr/palet/dentas_palet/inst/scripts/main_dentas_palet.R`

``` bash
find_best_layout_for_palette()
``` 

# farklı palet boyutları için optimizasyon

## palet boyutlarını bir tablodan al

### palet boyutlarıyla diğer verileri join mi edeceğiz?

#### error: fits_in

		Error in coef_length * length : non-numeric argument to binary operator
		2: f04 %>% dplyr::mutate(lhs_val = coef_length * length + coef_width *
					 width, rhs_val = coef_pal_length * palette_length + coef_pal_width *
				... at find_layout.R#13

opt01: comment fits_in_palette

			#, fits_in_palette = (coef_length * length + coef_width * width) <= (coef_pal_length * palette_length + coef_pal_width * palette_width)

opt02:

		str(f04)

		Classes ‘tbl_df’, ‘tbl’ and 'data.frame':       240 obs. of  12 variables:
		 $ formula        : chr  "=IF(D1<=B3,1,0)" "=IF(D2<=B4,1,0)" "=IF(D1*2<=B3,1,0)" "=IF(D2<=B4,1,0)" ...
		 $ layout_no      : chr  "101" "101" "201" "201" ...
		 $ layout_id      : chr  "01A" "01A" "02A" "02A" ...
		 $ expr_id        : int  1 2 4 5 7 8 10 11 13 14 ...
		 $ expr           : chr  "D1<=B3" "D2<=B4" "D1*2<=B3" "D2<=B4" ...
		 $ lhs            : chr  "D1" "D2" "D1*2" "D2" ...
		 $ op             : chr  "<=" "<=" "<=" "<=" ...
		 $ rhs            : chr  "B3" "B4" "B3" "B4" ...
		 $ coef_length    : num  1 0 2 0 1 0 0 1 0 1 ...
		 $ coef_width     : num  0 1 0 1 0 2 2 0 3 0 ...
		 $ coef_pal_length: num  1 0 1 0 1 0 1 0 1 0 ...
		 $ coef_pal_width : num  0 1 0 1 0 1 0 1 0 1 ...

Problemin sebebi: scope issue

		length = max(cardboard_side_a, cardboard_side_b)
		width = min(cardboard_side_a, cardboard_side_b)
		->
		cardboard_length = max(cardboard_side_a, cardboard_side_b)
		cardboard_width = min(cardboard_side_a, cardboard_side_b)

#### önce cl ve pl boyutlarını da ekleyelim tablolara

Check `~/projects/itr/palet/dentas_palet/inst/extdata/cardboards.tsv`

##### isim değişiklikleri

``` bash
function! RenameVars()
	%s/\<coef_length\>/coef_clength/g
	%s/\<cardboard_length\>/clength/g
	%s/\<coef_width\>/coef_cwidth/g
	%s/\<cardboard_width\>/cwidth/g
	%s/\<coef_pal_length\>/coef_plength/g
	%s/\<palette_length\>/plength/g
	%s/\<coef_pal_width\>/coef_pwidth/g
	%s/\<palette_width\>/pwidth/g
endfunction
``` 

##### tablonun formu nasıl olmalı?

		| coef_clength | coef_cwidth | coef_plength | coef_pwidth | lhs_val | rhs_val | fits_in_palette |
		| 1            | 0           | 1            | 0           | 400     | 1200    | 1               |
		| 0            | 1           | 0            | 1           | 300     | 1000    | 1               |

şu kolonları da ekleyelim:

		clength
		cwidth
		plength
		pwidth

bunlar cardboards.tsv ve palettes.tsv dosyalarından okunacak. 

crossjoin yapılacak.

## fits_in formülünü tekrar çalıştır

### bir tanesi için bunu yap

error: group_by tuhaf bir şekilde çalışıyor

#### toy problem üzerinde test et

Check `~/projects/study/r/dplyr_group_by.md`

#### sadece 101 ve 203 layout kalıyor. doğru mu?

``` bash
cardboard_id	palette_id	layout_no	fits_in_all
1	1	101	1
1	1	203	1
``` 

hesaplamalara bakalım

yeni hesaplama:

		1	1	901	09A	1300	600	FALSE
		1	1	901	09A	900	500	FALSE
		1	1	901	09A	800	500	FALSE

eski hesaplama: `~/projects/itr/palet/dentas_palet/out/out05_notable.tsv`

		901	09A	1300	1200	FALSE
		901	09A	900	1000	TRUE
		901	09A	800	1000	TRUE

rhs value'lar arasındaki fark neden kaynaklanıyor?

Doğrusu 1200 ve 1000 olması.

Fakat `/Users/mertnuhoglu/projects/itr/palet/dentas_palet/out/out05.tsv` içinde de yanlışlık var:

Bu datayı plength ve pwidth içinden alıyor. Bu da kaynak veriyi `~/projects/itr/palet/dentas_palet/inst/extdata/palettes.tsv` içinden alıyor.

Buradan düzeltelim

``` bash
	clength = 400
	cwidth = 300
	length = max(clength, cwidth)
	width = min(clength, cwidth)
	plength = 1200
	pwidth = 1000

	d1f05 = d1f04 %>%
		dplyr::mutate(
			lhs_val = coef_clength * clength + coef_cwidth * cwidth
			, rhs_val = coef_plength * plength + coef_pwidth * pwidth
			, fits_in_palette = (coef_clength * clength + coef_cwidth * cwidth) <= (coef_plength * plength + coef_pwidth * pwidth)
		)
	rio::export(d1f05, "out/d1out05.tsv")
	d1f05_notable = d1f05 %>%
		dplyr::select(layout_no, layout_id, lhs_val, rhs_val, fits_in_palette) %>%
		dplyr::arrange(layout_no)
	rio::export(d1f05_notable, "out/d1out05_notable.tsv")
	d1f06 = d1f05 %>%
		dplyr::group_by(layout_no, layout_id) %>%
		dplyr::summarize(fits_in_all = min(fits_in_palette)) %>%
		dplyr::filter(fits_in_all == 1)
	best_layout = best_layout(d1f06)
  ##>   layout_no layout_id fits_in_all
  ##>   <chr>     <chr>           <int>
  ##> 1 906       09F                 1
``` 

Buna ait hesaplar: `~/projects/itr/palet/dentas_palet/out/d1out05_notable.tsv`

		906	09F	1200	1200	TRUE
		906	09F	900	1000	TRUE

Formüller: `~/projects/itr/palet/dentas_palet/out/d1out05.tsv`

		=IF(D1*3<=B3,1,0)	906	09F	151	D1*3<=B3	D1*3	<=	B3	3	0	1	0	1200	1200	TRUE
		=IF(D2*3<=B4,1,0)	906	09F	152	D2*3<=B4	D2*3	<=	B4	0	3	0	1	900	1000	TRUE

``` bash
	p0 = readr::read_tsv(system.file("extdata", "palettes.tsv", package = "dentaspalet")) %>%
		dplyr::mutate(crossjoin = 1)
	c0 = readr::read_tsv(system.file("extdata", "cardboards.tsv", package = "dentaspalet")) %>%
		dplyr::mutate(crossjoin = 1) %>%
		dplyr::mutate(
			clength = dplyr::case_when(
        cardboard_side_a >= cardboard_side_b ~ cardboard_side_a
        , cardboard_side_a < cardboard_side_b ~ cardboard_side_b
			)
			, cwidth = dplyr::case_when(
        cardboard_side_a >= cardboard_side_b ~ cardboard_side_b
        , cardboard_side_a < cardboard_side_b ~ cardboard_side_a
			)
		) %>%
		dplyr::select(-cardboard_side_a, -cardboard_side_b)
	f04b = f04 %>%
		dplyr::mutate(crossjoin = 1) %>%
		dplyr::full_join(c0, by = "crossjoin") %>%
		dplyr::full_join(p0, by = "crossjoin") %>%
		dplyr::select(-crossjoin)
	f05 = f04b %>%
		dplyr::mutate(
			lhs_val = coef_clength * clength + coef_cwidth * cwidth
			, rhs_val = coef_plength * plength + coef_pwidth * pwidth
			, fits_in_palette = (coef_clength * clength + coef_cwidth * cwidth) <= (coef_plength * plength + coef_pwidth * pwidth)
		) %>%
		dplyr::arrange(cardboard_id, palette_id, layout_no)
	f05b = f05 %>%
		dplyr::select(cardboard_id, palette_id, layout_no, layout_id, lhs_val, rhs_val, fits_in_palette) %>%
		dplyr::arrange(cardboard_id, palette_id, layout_no)
	rio::export(f05, "out/out05.xlsx")
	rio::export(f05, "out/out05.tsv")
	rio::export(f05b, "out/out05b.tsv")
``` 

		palette_id	plength	pwidth
		p7	1200	1000

		cardboard_id	cardboard_side_a	cardboard_side_b
		cb1	400	300

Formüller: `~/projects/itr/palet/dentas_palet/out/out05.tsv`

		=IF(D1*3<=B3,1,0)	906	09F	151	D1*3<=B3	D1*3	<=	B3	3	0	1	0	cb1	400	300	p7	1200	1000	1200	1200	TRUE
		=IF(D2*3<=B4,1,0)	906	09F	152	D2*3<=B4	D2*3	<=	B4	0	3	0	1	cb1	400	300	p7	1200	1000	900	1000	TRUE

Sonuçlar `/Users/mertnuhoglu/projects/itr/palet/dentas_palet/out/out05b.tsv`

		cb1	p7	906	09F	1200	1200	TRUE
		cb1	p7	906	09F	900	1000	TRUE

Tamam sonuçlar doğru.

Şimdi bunu fits_in ile çekebilir miyim?

``` bash
	f06a = f05 %>%
		dplyr::filter(cardboard_id == "cb1", palette_id == "p7") %>%
		dplyr::select(cardboard_id, palette_id, layout_no, layout_id, fits_in_palette)
	rio::export(f06a, "out/out06a.tsv")
	f06b = f06a %>%
		dplyr::group_by(cardboard_id, palette_id, layout_no) %>%
		dplyr::summarize(fits_in_all = min(fits_in_palette)) %>%
		dplyr::filter(fits_in_all == 1) 
	rio::export(f06b, "out/out06b.tsv")
``` 

Sonuçlar: `~/projects/itr/palet/dentas_palet/out/out06a.tsv`

		cb1	p7	906	09F	TRUE
		cb1	p7	906	09F	TRUE

min: `~/projects/itr/palet/dentas_palet/out/out06b.tsv`

		cb1	p7	906	1

``` bash
best_layout(f06b)
  ##>   cardboard_id palette_id layout_no fits_in_all
  ##>   <chr>        <chr>      <chr>           <int>
  ##> 1 cb1          p7         906                 1
``` 

Şimdi bunu jenerikleştirelim.

``` bash
	h06a = f05 %>%
		dplyr::select(cardboard_id, palette_id, layout_no, layout_id, fits_in_palette)
	rio::export(h06a, "out/outh06a.tsv")
	h06b = h06a %>%
		dplyr::group_by(cardboard_id, palette_id, layout_no) %>%
		dplyr::summarize(fits_in_all = min(fits_in_palette)) %>%
		dplyr::filter(fits_in_all == 1) 
	rio::export(h06b, "out/outh06b.tsv")
``` 

Sonuçlar: `~/projects/itr/palet/dentas_palet/out/outh06b.tsv`

		cb1	p7	906	1

## best_layout fonksiyonu hepsi için çalıştır

### önce hepsinin israf oranını bul

Bunun için hangi verilere ihtiyacım var?

		area_cardboard = carea
		area_covered
		area_palette = parea

Bunun için de, her bir layout içinde kaç tane karton var, onu bilmeliyim.

Bunun için bir tablo oluşturalım: layouts

#### layouts tablosu hazırlama

Bunda layout_no, layout_id ve layout içindeki karton sayısı olacak.

Check `~/projects/itr/palet/dentas_palet/inst/extdata/layouts.tsv`

It contains `cardboard_per_layout`

#### best_layout ilk başta yine aynı şekilde en yüksek layout_no'ya göre bulunsun

sonra her bir grup cardboard_id için tüm paletlerin arasından en yüksek area_covered olan paleti bulsun.

cardboard_id, palette_id, layout_no üçlüsü için area_covered değerini ayrı bir tabloda tut.

sonra bunları h06b.tsv dosyasına ekle


