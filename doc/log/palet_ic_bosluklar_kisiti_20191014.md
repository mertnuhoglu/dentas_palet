
# palet: iç boşluklar kısıtını ekleme 20191014 

Tümer:

>  Palet eninden 5cm’den fazla içerde kalmamalıyız. İki taraftan toplam
>
>  Palet boyundan 15cm’den fazla içerde kalmamalıyız. İki taraftan toplam

Mert:

> bir şey sorucam yalnız: bu 5 cm'i şöyle hesaplıycam değil mi:
> 
> mesela, paletin eni 100 cm olsun.
> biz de 3 tane karton koyduk, bunlar da 30'ar santim eninde olsun. 30 + 30 + 30 toplıycam. 90 çıktı.
> 100'den 90'ı çıkartıcam, 10.
> 10 > 5. o zaman infizibl. 
> mantık böyle olacak değil mi?

## opt01: lhs ve rhs yanına bir kolon ekle kısıt kontrolünü göster

Edit `~/projects/itr/palet/dentas_palet/R/find_layout.R`

``` bash
			, gap_in_length = plength - clength
			, gap_in_width = pwidth - cwidth
			, fits_gap = ((gap_in_length  <= 15) & (gap_in_width <= 5))
``` 

``` bash
fits_in = function(f04b) {
	f05 = f04b %>%
		dplyr::mutate(
			lhs_val = coef_clength * clength + coef_cwidth * cwidth
			, rhs_val = coef_plength * plength + coef_pwidth * pwidth
			, fits_in_palette = lhs_val <= rhs_val
			, fits_overflow = lhs_val <= (rhs_val + 100)
			, gap_in_length = plength - clength
			, gap_in_width = pwidth - cwidth
			, fits_gap = ((gap_in_length  <= 15) & (gap_in_width <= 5))
		) %>%
		dplyr::arrange(cardboard_id, palette_id, dplyr::desc(layout_id), formula_id) %>%
		dplyr::select(cardboard_id, palette_id, layout_id, layout_no, formula_id, dplyr::everything()) 
	f05b = f05 %>%
		dplyr::select(cardboard_id, palette_id, layout_no, layout_id, lhs_val, rhs_val, fits_in_palette, fits_overflow, fits_gap) %>%
		dplyr::arrange(cardboard_id, palette_id, layout_no)
	h06a = f05 %>%
		dplyr::select(cardboard_id, palette_id, layout_no, layout_id, fits_in_palette, fits_overflow, fits_gap)
	h06b = h06a %>%
		dplyr::group_by(cardboard_id, palette_id, layout_no) %>%
		dplyr::summarize( 
			fits_in_all = min(fits_in_palette)
			, fits_gap_all = min(fits_gap)
		) %>%
		dplyr::mutate(overflow_allowed = FALSE)
	h06c = h06b %>%
		dplyr::filter((fits_in_all == 1) & (fits_gap_all == 1)) 
	h06bof = h06a %>%
		dplyr::group_by(cardboard_id, palette_id, layout_no) %>%
		dplyr::summarize( 
			fits_in_all = min(fits_in_palette)
			, fits_gap_all = min(fits_gap)
		) %>%
		dplyr::mutate(overflow_allowed = TRUE)
	h06cof = h06bof %>%
		dplyr::filter((fits_in_all == 1) & (fits_gap_all == 1)) 
	h06d = h06c %>%
		dplyr::bind_rows(h06cof)
	dir.create(path = "www/log", recursive = T)
	rio::export(f05, "www/log/f05.tsv")
	rio::export(f05b, "www/log/f05b.tsv")
	rio::export(h06a, "www/log/h06a.tsv")
	rio::export(h06b, "www/log/h06b.tsv")
	rio::export(h06c, "www/log/h06c.tsv")
	rio::export(h06bof, "www/log/h06bof.tsv")
	rio::export(h06cof, "www/log/h06cof.tsv")
	rio::export(h06d, "www/log/h06d.tsv")
	return(h06d)
}
``` 

## sağlama: 53N96-286

Error: hepsi için fits_gap FALSE çıkmış

Sebep: katsayılarla çarpmamışım

Çözüm:

Tek tek tüm çizimlerin üzerinden geçip katsayıları toplamalıyım

Alternatif olarak: mevcut parse ettiğim formüllerin şeklini düzenleyebilirim

## yatay dikey uzunluk ve en katsayı formüllerini oluştur

opt01: iki adımda yapıcaz

mutate:

		ccl ccw cpl cpw -> ccl ccw ps
                         1   0 pl/pw

pivot_wider:

    ccl ccw ps -> hcl hcw vcl vcw ps

Nerede yapalım bu işlemleri?

		opt01: f05'ten sonra f05b'den önce

### 01: create axis column

mutate ifelse nasıldı?

``` bash
	f05a1 = f05 %>%
		dplyr::mutate( axis = ifelse(coef_plength == 1, "length", "width") )
``` 

f05'in sıralaması nasıl?

### 02: pivot_wider

Hedef:

``` bash
	| cl | cw | axis |  |
	| 1  | 0  | hor  |  |
	| 0  | 1  | ver  |  |
	| 2  | 0  | hor  |  |
	| 0  | 1  | ver  |  |
``` 

-> pivot_wider			

``` bash
	| hor_cl | hor_cw | ver_cl | ver_cw |
	| 1      | 0      | 0      | 1      |
	| 2      | 0      | 0      | 1      |
``` 

pivot_wider konusunu tekrar bir hatırlayalım.

#### other: isim değişikliği:

``` bash
acksed -n ex_gather_spread_group_separate_unnest.Rmd study_tidyr_pivot.md
cd ~/projects/study/r/
git mv ex_gather_spread_group_separate_unnest.Rmd study_tidyr_pivot.md
``` 

---

``` bash
	f05a1 = f05 %>%
		dplyr::mutate( axis = ifelse(coef_plength == 1, "hor", "ver") )
	f05a2 = f05a1 %>%
		tidyr::pivot_wider(names_from = "axis", values_from = c("coef_clength", "coef_cwidth"))
``` 

refactoring: isimleri kısalt: coef_clength -> ccl

``` bash
%s/\<coef_clength\>/ccl/g
%s/\<coef_cwidth\>/ccw/g
``` 

kolonların sıralamasını da değiştir

boşlukları 0 ile doldur

``` bash
ccl_ver	ccl_hor	ccw_ver	ccw_hor
0	2	0	0
0	0	1	0
``` 

#### Neden tek satıra indirgemedi?

opt
	
		oyuncak problemde test et
		vignette örneğini incele

hipotez: belki diğer kolonlarla karışık olduğundan olabilir

opt01: oyuncak problemde test et

tibble ile örnek veriyi oluştur

Ref: `ex02: oyuncak problemde çok kolondan values_from <url:/Users/mertnuhoglu/projects/study/r/study_tidyr_pivot.md#tn=ex02: oyuncak problemde çok kolondan values_from>`

Neden burada da aynı durum çıktı?

opt02: formula_id olmasın

Bu şekilde düzgün çalışıyor.

opt03: cpl_id kullan aynı olacak şekilde

Bu da düzgün çalışıyor. O zaman bunu kullanmalıyız.

#### sadece tekil bir cpl id kalsın fazla kolon olarak sonra pivot_wider yap

Önce sadece formula'ları tutacağım bir tablo oluştur: `cpl0` olsun ismi

Bunun için önce cpl_id oluşturmalıyım, cardboard_id, palette_id, layout_id kolonlarını birleştirip.

##### R ile birden çok kolonun değerleri nasıl birleştirilir?

unite?

refactoring: coef_plength -> cpl

``` bash
%s/\<coef_plength\>/cpl/g
%s/\<coef_pwidth\>/cpw/g
``` 

##### satırları yine indirgemedi

opt01: Küçük veriyle test et

``` bash
t0 = tibble::tribble(
	~cpl_id, ~ccl, ~ccw, ~cpl, ~cpw, ~axis,
	"53244-245:100*120:X4C", 1, 0, 0, 2, "ver",
	"53244-245:100*120:X4C", 0, 1, 2, 0, "ver",
	"53244-245:100*120:X4B", 1, 0, 2, 0, "ver",
	"53244-245:100*120:X4B", 0, 1, 0, 2, "ver"
)
t1 = t0 %>%
	tidyr::pivot_wider(names_from = "axis", values_from = c("ccl", "ccw"), values_fill = list(ccl = 0, ccw = 0))
``` 

Ok, cpl ve cpw olmamalıydı.

``` bash
t0 = tibble::tribble(
	~cpl_id, ~ccl, ~ccw, ~axis,
	"53244-245:100*120:X4C", 1, 0, "ver",
	"53244-245:100*120:X4C", 0, 1, "ver",
	"53244-245:100*120:X4B", 1, 0, "ver",
	"53244-245:100*120:X4B", 0, 1, "ver"
)
t1 = t0 %>%
	tidyr::pivot_wider(names_from = "axis", values_from = c("ccl", "ccw"), values_fill = list(ccl = 0, ccw = 0))
  ##>   cpl_id                    ccl_ver     ccw_ver
  ##>   <chr>                 <list<dbl>> <list<dbl>>
  ##> 1 53244-245:100*120:X4C         [2]         [2]
  ##> 2 53244-245:100*120:X4B         [2]         [2]
``` 

Uyarı verdi: 1: Values in `ccl` are not uniquely identified; output will contain list-cols.

her iki satırın da "ver" olmasından kaynaklanmış olabilir. çoklu paletlere has bir durum mu acaba?

``` bash
t0 = tibble::tribble(
	~cpl_id, ~ccl, ~ccw, ~axis,
	"53244-245:100*120:X4C", 1, 0, "ver",
	"53244-245:100*120:X4C", 0, 1, "hor",
	"53244-245:100*120:X4B", 1, 0, "ver",
	"53244-245:100*120:X4B", 0, 1, "hor"
)
t1 = t0 %>%
	tidyr::pivot_wider(names_from = "axis", values_from = c("ccl", "ccw"), values_fill = list(ccl = 0, ccw = 0))
  ##>   cpl_id                ccl_ver ccl_hor ccw_ver ccw_hor
  ##>   <chr>                   <dbl>   <dbl>   <dbl>   <dbl>
  ##> 1 53244-245:100*120:X4C       1       0       0       1
  ##> 2 53244-245:100*120:X4B       1       0       0       1
``` 

Tamam şimdi beklenen sonucu verdi.

##### Error: cpl object not found

opt

		Elle yap
		tsv dosyalarına bak o ana kadarki

f04b nerede?

Çözüm:

``` bash
	cpl1 = cpl0 %>%
		dplyr::mutate( axis = ifelse(cpl == 1, "hor", "ver") ) %>%
		dplyr::select(-cpl, -cpw)
``` 

---

Tamam, şu an 02A 02B gibi sadece 2 formülden oluşan layoutlar düzgün ayıklanıyor.

## 3 adetlik formülleri nasıl ele alacağız?

Örnek:

``` bash
cpl_id	ccl	ccw	cpl	cpw
53244-245:100*120:03B	1	1	1	0
53244-245:100*120:03B	0	2	0	1
53244-245:100*120:03B	1	0	0	1
``` 

Bunun şekline bakalım:

Mantığı değiştirmemiz gerekiyor.

		her bir satırı ele al
			herhangi bir satırda ccl + ccw <=1 ise ihmal et
			önceki mantığın aynısını uygula ancak farkları da koy

``` bash
	f05 = f04b %>%
		dplyr::mutate(
			...
			, axis = ifelse(cpl == 1, "hor", "ver") 
			, gap = rhs_val - lhs_val
			, gap_allowed = ifelse(axis == "hor", 15, 5)
			, single_cardboard = ccl + ccw == 1
			, fits_gap = ifelse(single_cardboard, TRUE, gap < gap_allowed)
``` 

2 üstü palet taşımalarında gap'i ihmal edelim.

``` bash
			, fits_gap = ifelse(single_cardboard | palette_per_layout > 1, TRUE, gap < gap_allowed)
``` 

## fits_gap ile filtrelemeyi yapalım
	



