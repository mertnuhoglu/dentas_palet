# dentaspalet

dentaspalet projesi Dentaş'ın paletlerine yüklenen kartonların yerleşimini optimize eder.

## git

``` bash
git clone git@bitbucket.org:mertnuhoglu/dentas_palet.git
``` 


## Setup nginx

Ref: `Run nginx <url:/Users/mertnuhoglu/projects/itr/vrp/vrp_doc/dentas/process_setup_dentas.Rmd#tn=Run nginx>`

Edit `nginx.conf` in `~/projects/itr/vrp/vrp/nginx/nginx.conf` for reverse proxy

        location / {
            proxy_pass http://dentaspalet.i-terative.com:5060;

Run nginx

``` bash
docker exec -it vrp_nginx-router_1 bash
certbot certonly --webroot -w /usr/share/nginx/html -d dentaspalet.i-terative.com 
docker-compose stop
docker-compose up
``` 

## Setup dentas_palet software

Run `~/projects/itr/palet/dentas_palet/inst/scripts/install_software.sh`

``` bash
cd ~/dentas_palet
make build
``` 

## Setup dentas_palet API on Windows 10

1. R platformu kurulumu

R kurulumu: https://cran.r-project.org/bin/windows/base/

RStudio kurulumu: https://rstudio.com/products/rstudio/download/#download

Rtools kurulumu: https://cran.r-project.org/bin/windows/Rtools/

2. RStudio programını açıp şu komutları çalıştırarak, ihtiyaç duyduğumuz R paketlerini yüklemeniz gerekiyor:

``` R
install.packages(c("dplyr", "readr", "glue", "jsonlite", "rjson", "WriteXLS", "roxygen2"))
install.packages(c("tidyr", "stringr", "rio"))
install.packages(c("Rook", "readxl", "plumber"))
install.packages("devtools")
install.packages(c("shiny", "rmarkdown", "shinydashboard", "usethis", "testthat"))
install.packages(c("sodium", "curl", "devtools", "shinyjs", "DT"))
install.packages(c("tidyr", "stringr", "rio"))
devtools::install_github("paulc91/shinyauthr")
``` 

3. Komut satırından projeyi indirin:

Öncesinde git kurulumu yapmış olmalısınız: https://www.atlassian.com/git/tutorials/install-git#windows

``` bash
git clone https://bitbucket.org/mertnuhoglu/dentas_palet/
cd dentas_palet
``` 

4. RStudio içinden projeyi açın ve API sunucusunu çalıştırın:

Rstudio > File > Create new project > From existing directory > (Proje klasörünü seçin)

Sunucuyu çalıştırmak için:

``` R
devtools::load_all() 
library(plumber)
pr <- plumber::plumb("app/palet_api.R")
stat = PlumberStatic$new("./www")
pr$mount("/www", stat)
pr$run(host="0.0.0.0", port=5061)
``` 

Şu an API sunucu çalışıyor olmalı. Test etmek için, bu komutu kullanabilirsiniz (palettes_02.csv ve cardboards_05.csv dosyalarının bulunduğu klasörden):

``` bash
cd /c/Users/administrator/dentas_palet/inst/extdata
curl -v -F overflow_max=100 -F gap_length=150 -F gap_width=50 -F palettes=@"palettes_02.csv" -F cardboards=@"cardboards_05.csv" http://localhost:5061/best_layout
curl http://localhost:5061/www/report.csv -o report.csv
``` 

# Run dentas_palet app

Run `~/projects/itr/palet/dentas_palet/app/server.R`

``` bash
R --vanilla
``` 

Shiny web server

``` r
devtools::load_all() 
shiny::runApp("app", host="0.0.0.0", port = 5060)
``` 

API server:

``` r
devtools::load_all() 
library(plumber)
pr <- plumber::plumb("app/palet_api.R")
stat = PlumberStatic$new("./www")
pr$mount("/www", stat)
pr$run(host="0.0.0.0", port=5061)
``` 

Open in browser: 

		https://dentaspalet.i-terative.com
		http://localhost:5060

Upload input files:

		/Users/mertnuhoglu/projects/itr/palet/dentas_palet/inst/extdata/palettes.tsv
		/Users/mertnuhoglu/projects/itr/palet/dentas_palet/inst/extdata/cardboards.tsv

		/Users/mertnuhoglu/gdrive/mynotes/prj/itr/iterative_mert/musteriler/dentas_palet/palettes_01.xlsx
		/Users/mertnuhoglu/gdrive/mynotes/prj/itr/iterative_mert/musteriler/dentas_palet/cardboards_03.xlsx

# Tests

## Test scripts to reproduce data

Check `~/projects/itr/palet/dentas_palet/tests/testthat/test-palette_layout_per_cardboard.R`

Run test scripts interactively. Source init functions manually.

## Run automated tests

``` r
devtools::test()
``` 

# Documentation

Müşterinin bize verdiği doküman: `/Users/mertnuhoglu/gdrive/mynotes/prj/itr/iterative_mert/musteriler/dentas_palet/mopal_20181204.xlsx`

